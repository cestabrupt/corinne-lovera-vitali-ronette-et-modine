# ~/ABRÜPT/CORINNE LOVERA VITALI/RONETTE ET MODINE/*

La [page de ce livre](https://abrupt.ch/corinne-lovera-vitali/ronette-et-modine/) sur le réseau.

## Sur le livre

De ce petit livre on pourrait dire qu’il gomme et dégomme les grands hommes, mais ce n’est pas ça. Il donne et redonne plutôt.

Il donne une place à des femmes écrasées par lesdits grands hommes et dont on ignore même les noms. Il redonne les noms de grandes femmes qui ne se sont pas laissé écraser.

Il donne de la voix aussi. Le plus loin possible de l’autorité et de la « culture-phallus ». Au plus près des êtres et de la nature-ventre. Avec.

Le <a href="https://abrupt.ch/corinne-lovera-vitali/ronette-et-modine#son">parlé musique</a> de ronette et modine, par corinne lovera vitali & fernand fernandez. 

## Sur l'auteur

Corinne Lovera Vitali s’appelle clv et tout ou presque de son travail se trouve sur son <a href="http://corinne-lovera-vitali.net/" >site</a> et son <a href="https://soundcloud.com/loveravitali" >site</a> et son <a href="https://www.youtube.com/channel/UCG5IxtYDHLWltY9KGUTFbpQ" >site</a> où tout est gratuit ce qui est presque de l’anti-tout.

Ses livres pas gratuits sont publiés depuis 1999 en voici quelques-uns complètement pas dans l’actualité la preuve celui-ci est épuisé : <a href="http://www.lmda.net/din/tit_lmda.php?Id=52355" >la Taille des hommes</a>, celui-ci est stocké à la maison : <a href="http://non.ultra-book.com/portfolio#scrute_le_travail_clv__61788.jpg" >Scrute le travail</a>, mais ceux-là les derniers en date oui ils peuvent encore payer des <a href="https://www.lerouergue.com/auteurs/lovera-vitali-corinne" >croquettes</a> : <a href="https://www.publie.net/2016/10/19/nouveaute-ce-quil-faut-de-corinne-lovera-vitali/" >Ce qu’il faut</a> et <a href="http://media.wix.com/ugd/e20eec_a018f92529174dcb8a8c148a5041d061.pdf" >78 moins 39</a>.

Elle écrit parfois des textes tout isolés comme des taupes, qui parfois voient le jour comme dans la série <a href="http://www.t-pas-net.com/libr-critique/tag/vitali-cest-la-valise/" >C'est la valise</a>, ou ici <a href="http://remue.net/spip.php?article7775" >l’apparadis</a> ou là <a href="http://www.tapin2.org/auto" >auto</a>, et parfois non je préfère les garder taupes.
 
Sinon je suis bien occupée à faire le <a href="https://soundcloud.com/les-fernandez-les-fernandez" >parlé musique</a> avec <a href="https://www.fernandfernandez.org/" >Fernand Fernandez.</a> La plupart du temps nous nous tenons loin de la foule déchaînée, et <a href="https://lesfernandez.bandcamp.com/track/via" >quelquefois il y a du monde.</a>

Voici <a href="https://www.dropbox.com/s/jzk87fqmhm398wf/clv%20b-b%20WEB.pdf?dl=0" >la grosse récap.</a>

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
